import React, {FunctionComponent, ReactElement, useState} from 'react';
import {Button, Dimensions, Modal, StyleSheet, Text, View} from 'react-native';

type Props = {
    children?: ReactElement;
    visibility: boolean;
}

const {height, width} = Dimensions.get('window')

const Popup: FunctionComponent<Props> = ({children, visibility}) => {

    return(
        <Modal animationType={'slide'} transparent={true} visible={visibility}>
            <View style={styles.modal}>
                <View style={styles.modalContent}>
                    {children}
                </View>
            </View>
        </Modal>
    )
}

export default Popup

const styles = StyleSheet.create({
    modal: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.3)'
    },
    modalContent: {
        borderRadius: 4,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 3
        },
        minWidth: width - 100
    }
});
