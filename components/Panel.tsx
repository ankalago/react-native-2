import React, {FunctionComponent} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';

type Props = {
    showFilter: Function
}

const Panel: FunctionComponent<Props> = ({ showFilter }) => {

    return(
        <View style={styles.panel}>
            <Button onPress={() => showFilter('list')} title="Lista" />
            <Text>|</Text>
            <Button onPress={() => showFilter('points')} title="Mostrar / Ocultar" />
        </View>
    )
}

export default Panel

const styles = StyleSheet.create({
    panel: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
