import React, {FunctionComponent} from 'react';
import {StyleSheet, TextInput, View, Text} from 'react-native';

type Props = {
    title?: string;
}

const Input: FunctionComponent<Props> = ({title, ...rest}) => {
    return(
        <View style={styles.wrapper}>
            <Text>{title}</Text>
            <TextInput {...rest} />
        </View>
    )
}

export default Input

const styles = StyleSheet.create({
    wrapper: {
        height: 40
    },
});
