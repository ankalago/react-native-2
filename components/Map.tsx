import MapView, {MapEvent} from 'react-native-maps';
import React, {FunctionComponent, ReactElement} from 'react';
import {Dimensions, StyleSheet} from 'react-native';

type Props = {
    style?: any;
    children?: ReactElement;
    longPress?: Function;
}

const {height, width} = Dimensions.get('window')

const Map: FunctionComponent<Props> = ({style, children, longPress}) => {
    return(
        <MapView style={styles.map} onLongPress={(e: MapEvent) => longPress ? longPress(e) : null}>
            {children}
        </MapView>
    )
}

export default Map

const styles = StyleSheet.create({
    map: {
        flex: 3,
        height,
        width
    },
});
