import {StatusBar} from 'expo-status-bar';
import React, {useCallback, useMemo, useReducer, useState} from 'react';
import {StyleSheet, Text, View, Dimensions, Button, FlatList} from 'react-native';
import Map from './components/Map'
import Popup from './components/Popup';
import Panel from './components/Panel';
import {Marker} from 'react-native-maps';
import Input from './components/Input';

const {height, width} = Dimensions.get('window')

const initialState = {
    cont: 0
}

const reducer = (state, action) => {
    switch (action.type) {
        case 'increment':
            return {
                ...state,
                cont: state.cont + 1
            }
        case 'decrement':
            return {
                ...state,
                cont: state.cont - 1
            }
        default:
            return state;
    }
}

const users = [
    {name: 'pool', age: 3},
    {name: 'pool1', age: 4}
];

export default function App() {

    const [points, setPoints] = useState([]);
    const [pointTemp, setPointTemp] = useState({});
    const [name, setName] = useState('');
    const [visibility, setVisibility] = useState(false);
    const [visibilityFilter, setVisibilityFilter] = useState('form');
    const [visibilityPoints, setVisibilityPoints] = useState(true);
    const [count, setCount] = useState(0);

    const [state, dispatch] = useReducer(reducer, initialState) // TODO similar a redux
    const totalAge = useMemo(() => { // TODO se ejecuta una sola vez a menos que [users] cambie
        let age = 0
        console.log('calculando...');
        users.forEach(user => {
            age = age + user.age
        })
        return age
    }, [users])

    const handleLongPress = (point: any) => {
        const {nativeEvent} = point
        setPointTemp({...nativeEvent.coordinate})
        setVisibility(true)
    }

    const handleChangeText = (text: string) => {
        console.log('***************** useCallback: ');
        setName(text)
    }

    const addPoint = () => {
        const newVar: any = [...points, {coordinate: pointTemp, title: name}];
        setPoints(newVar)
        setVisibility(false)
        setName('')
    }

    const addCount = useCallback(() => { // todo useCallback t edevuelve la referencia de la misma función se usa para aumentar performance
        console.log('***************** useCallback: ');
        setCount(count + 1)
    }, [count])

    const form = () => {
        return (
            <>
                <View style={styles.formInput}>
                    <Input title="Nombre" placeholder="Nompre del punto" onChangeText={handleChangeText}/>
                </View>
                <View style={styles.formButton}>
                    <Button onPress={addPoint} title="Aceptar"/>
                </View>
                <View>
                    <Button onPress={() => dispatch({type: 'increment'})} title="Incrementar" />
                    <Text style={{textAlign: 'center'}}>{state.cont}</Text>
                    <Button onPress={() => dispatch({type: 'decrement'})} title="Decrementar" />
                </View>
                <View style={{backgroundColor: '#ccc'}}>
                    <Text style={{textAlign: 'center'}}>{count}</Text>
                    <Button onPress={addCount} title="Incrementar" />
                </View>
            </>
        )
    }

    const resetVisibility = () => {
        setVisibility(false)
        setVisibilityFilter('form')
    }

    const list = (points: any) => {
        return (
            <>
                <View style={styles.list}>
                    <FlatList
                        data={points}
                        keyExtractor={(item, index) => String(index)}
                        renderItem={({item, index}) => <View style={styles.listItem}><Text
                            key={index}>{item.title}</Text></View>}/>
                </View>
                <View style={styles.listButton}>
                    <Button onPress={resetVisibility} title="Cerrar"/>
                </View>
            </>
        )
    }

    const handleVisibleFilter = (filter: string) => {
        if (filter === 'list') {
            setVisibilityFilter(filter)
            setVisibility(true)
        } else {
            setVisibilityPoints(!visibilityPoints)
        }
    }

    console.log('***************** totalAge: ', totalAge);

    return (
        <View style={styles.container}>
            <StatusBar style="auto"/>
            <Popup visibility={visibility}>
                {
                    visibilityFilter === 'form' ?
                        form() :
                        list(points)
                }
            </Popup>
            <Map longPress={handleLongPress}>
                {visibilityPoints && points.map((point, index) => <Marker coordinate={point.coordinate} key={index} title={point.title}/>)}
            </Map>
            <Panel showFilter={handleVisibleFilter}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    list: {
        height: height - 250
    },
    listItem: {
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
        justifyContent: 'center',
        padding: 20
    },
    listButton: {
        paddingBottom: 15
    },
    formInput: {
        padding: 20
    },
    formButton: {
        paddingBottom: 15
    },
});
